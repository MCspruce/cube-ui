# UI 使用指南
[返回主页](../#cube-ui-文档)

## 容器辅助按钮
**丢弃物品按钮【箱子图标】**
<br>选定物品后点击该按钮丢弃选中的一个物品
<br>**丢弃全部物品按钮【箱子图标，有加号】**
<br>选定物品后点击该按钮丢弃选中的全部物品
<br>**摧毁物品按钮【火焰图标】**
<br>[创造模式可见] 选定物品后点击该按钮摧毁选中的一个物品
<br>**摧毁全部物品按钮【火焰图标，有加号】**
<br>[创造模式可见] 选定物品后点击该按钮摧毁选中的全部物品
<br>**帮助/配方按钮【指南针图标】**
<br>点击查看该容器的帮助/配方
<br>**切换模式**
<br>默认 - 无效果
<br>自动 - 选中物品自动填充至空槽位(自动抢箱/快速移动)
<br>丢弃 - 选中物品自动丟弃全部

## 跨设备/存档复制建筑结构
> **提示**
<br>请确保你有管理员权限！！！

需要用到的附加包：[简单工具（点击下载）](https://gitlab.com/MCspruce/cube-ui/-/raw/main/subpack/Cube_UI_Dawn.mcpack)

导出
1. 在聊天栏输入`/give @s structure_block`获得结构方块
2. 放置结构方块并用结构方块框选建筑结构
3. 填好左边的结构方块名称并记住
4. 点击右下角的"导出"按钮
5. 将.mcstructure文件放在"简单工具"(Cube UI附加包)的`structures`文件夹中

导入
1. 将"简单工具"移动到`(前面的路径因系统而异)/game/com.mojang/behavior_packs`文件中
2. 进入Minecraft并在需要导入的存档激活"简单工具"附加包
3. 进入世界
4. 在聊天栏输入`/give @s structure_block`获得结构方块
5. 放置结构方块并设置结构方块为加载模式
6. 填好刚刚输入的结构方块名称
7. 调好结构方块要加载的区域
8. 点击加载

> **注意：**
> * 第二次导入不用移动简单工具和激活简单工具，跳过此步即可！(除非不是之前导入过的存档)
> * 第二次导出移动.mcstructure文件时。
    需要在minecraftWorld文件夹搜索"简单工具"，
    再将.mcstructure文件导入至简单工具的structures文件夹中！(除非不是之前导入过的存档)