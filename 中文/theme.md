# UI 自定义教程
[返回主页](../#cube-ui-文档)

你需要下载 [自定义资源包](https://gitlab.com/MCspruce/cube-ui/-/raw/main/subpack/set.mcpack) 进行个性定制 Cube UI。

## 自定义教程

！支持的音频文件格式：.ogg .fsb
<br>！最佳的图片文件格式：.png .jpg
<br>！尽量将文件格式转化为以上所提及的格式

### 修改点击音效
1. 打开自定义资源包的 cube_ui/sounds/ 目录
2. 将想要的点击音效放入该目录
3. 删除原来的 click.ogg 文件
4. 将想要的点击音效文件重命名为 click.ogg

### 修改提示音效
1. 打开自定义资源包的 cube_ui/sounds/ 目录
2. 将想要的提示音效放入该目录
3. 删除原来的 toast.ogg 文件
4. 将想要的点击音效文件重命名为 toast.ogg

### 修改背景音乐
1. 打开自定义资源包的 sounds/music/menu/ 目录
2. 将想要的背景音乐放入该目录
3. 将想要的背景音乐文件重命名为 menu1.ogg
4. 随后也可以添加 menu2.ogg menu3.ogg menu4.ogg 到此目录

### 修改背景图片
1. 打开自定义资源包的 textures/ui/ 目录
2. 将想要的背景图片放入该目录
3. 将想要的背景图片文件重命名为 panorama_overlay.png 或 panorama_overlay.jpg（保留原有后缀）

### 修改背景图片（模糊）
1. 打开自定义资源包的 textures/ui/ 目录
2. 将想要的模糊背景图片放入该目录
3. 将想要的模糊背景图片文件重命名为 panorama_blur.png 或 panorama_blur.jpg（保留原有后缀）

### 修改变量
1. 打开自定义资源包的 ui/_global_variables.json 文件
2. 修改某些变量的值

## 保存更改并应用

！如需再次修改自定义资源包并导入游戏，
<br>  需要进入游戏 > 设置 > 存储 > Cube UI 自定义资源包 > 删除 再进行导入自定义资源包操作。

1. 来到自定义资源包的 manifest.json 文件所在的目录
2. 选中该目录所有文件及文件夹
3. 压缩选中的文件及文件夹
4. 修改命名压缩后的压缩包的后缀名为 .mcpack
5. 将 .mcpack 导入 Minecraft
6. 进入 Minecraft 并激活 Cube UI 自定义资源包
