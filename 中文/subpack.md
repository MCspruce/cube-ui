# 下载扩展包
[返回主页](../#cube-ui-文档)

扩展包，可以用于增强 Cube UI 功能，改变主题颜色…
<br>现在，选择合适你的扩展包吧~

> **提示**
<br>资源激活排序为 `扩展包 > Cube UI > 其他扩展包` 
<br>（从左边开始为最高排序）

**黎明主题**
<br>作者：MC_spruce
<br>特色：给 Cube UI 带来不一样的黑暗主题吧
<br>链接：[点击下载](https://gitlab.com/MCspruce/cube-ui/-/raw/main/subpack/Cube_UI_Dawn.mcpack)

**DelicPixel_Exten**
<br>作者：[EscapeBacteia](https://klpbbs.com/space-uid-15733.html)
<br>特色：这是个Cube UI的扩展包，兼于原版高清材质+Cube UI
<br>链接：[点击查看更多](https://klpbbs.com/thread-21680-1-1.html)

**专注面板**
<br>作者：MC_spruce
<br>特色：专注面板，是 MC_spruce 的 Cube UI V2.6 版本之前自带的功能，为了保持 Cube UI 的简洁和兼容，专注面板分离为扩展包。
<br>链接：[点击查看更多](https://klpbbs.com/thread-74800-1-1.html)

**粉色主题**
<br>作者：MC_spruce
<br>特色：给 Cube UI 带来不一样的粉色主题吧
<br>链接：[点击下载](https://gitlab.com/MCspruce/cube-ui/-/raw/main/subpack/Cube_UI_Pink.mcpack)

**自定义资源包**
<br>作者：MC_spruce
<br>特色：适用于深度定制 Cube UI 的用户，自定义 Cube UI 。
<br>链接：[点击下载](https://gitlab.com/MCspruce/cube-ui/-/raw/main/subpack/set.mcpack)
