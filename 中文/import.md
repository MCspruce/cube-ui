# 导入教程
[返回主页](../#cube-ui-文档)

> **注意事项**
> 1. 压缩包文件选择`"改后缀名"`或`"移动文件"`，文件夹选择`"移动文件"`
> 2. 以下方法**仅适用于Minecraft 基岩版**

## 一、删除旧版资源包部分

> **提示**
> <br>如果您曾经已导入Cube UI，
> <br>再次导入务必需要更新资源包！

按照以下流程删除旧版UI资源包
<br>`进入Minecraft＞设置＞存储＞资源包＞选择Cube UI＞点击删除`

## 二、检查环境
安卓设备检查Minecraft文件储存位置是否设为外部
<br>`Minecraft > 设置 > 存储 > 文件储存位置 > 外部`

Windows设备检查是否显示文件扩展名
<br> `桌面 > 我的电脑(计算机) > 组织 > 文件夹和搜索选项 > 查看 > 隐藏已知文件类型扩展名(必须关闭) > 应用`

检查是否有支持重命名的文件管理

检查文件是否完整
<br>* 至少有清单文件(manifest.json)
  
检查Minecraft版本是否大于或等于1.17
<br>* 没有请升级Minecraft

## 三、导入资源包部分
### mcpack后缀名直接导入（.mcaddon文件也可以）
#### Windows
方法一
<br>1 双击打开
<br>方法二
<br>1 右键要导入的文件
<br>2 打开方式
<br>3 选择Minecraft

#### ES文件浏览器
1 长按.mcpack文件
<br>2 点击更多
<br>3 点击打开
<br>4 选择Minecraft
<br>5 点击仅此一次

#### MT管理器
1 长按.mcpack文件
<br>2 点击打开方式…
<br>3 点击左下角的类型
<br>4 点击全部
<br>5 点击Minecraft

### 移动文件
#### windows
移动文件至`C:\Users\[用户名]\AppData\Local\Packages\Microsoft.MinecraftUWP_8wekyb3d8bbwe\LocalState\games\com.mojang\resource_packs`
* Windows系统一般默认用户名都是Admin(管理员)

#### android 或 HarmonyOS
移动文件至`/storage/emulated/0/Android/data/com.mojang.minecraftpe/files/games/com.mojang/resource_packs`

#### ios
文件移动至`我的iphone＞Minecraft＞games＞commojang＞resource_packs`

## 四、等待Minecraft提示导入成功

## 五、激活资源包
  **在全局启用**
<br>    `设置＞全局资源＞选择Cube UI资源包＞激活＞返回`
<br>  **在世界中启用**
<br>    `游戏＞编辑需要的世界＞资源包＞选择Cube UI资源包＞激活＞返回`