[广告拦截]
! Title: 广告拦截
! Version: 202301241831

! bilibili - 视频播放
bilibili.com###right-bottom-banner
bilibili.com###live_recommand_report > div.pl__card:last-child
bilibili.com###live_recommand_report > div.pl__head:first-child
bilibili.com###biliMainHeader > div.bili-header.fixed-header > div.bili-header__bar.mini-header:first-child > ul.left-entry:first-child > li.v-popover-wrap:nth-child(2) > a.default-entry
bilibili.com###biliMainHeader > div.bili-header.fixed-header > div.bili-header__bar.mini-header:first-child > ul.left-entry:first-child > li.v-popover-wrap:nth-child(5) > a.default-entry
bilibili.com###biliMainHeader > div.bili-header.fixed-header > div.bili-header__bar.mini-header:first-child > ul.left-entry:first-child > li.v-popover-wrap:nth-child(7)
bilibili.com###biliMainHeader > div.bili-header.fixed-header > div.bili-header__bar.mini-header:first-child > ul.left-entry:first-child > li.v-popover-wrap:nth-child(6)
bilibili.com###biliMainHeader > div.bili-header.fixed-header > div.bili-header__bar.mini-header:first-child > ul.right-entry:last-child > li.v-popover-wrap:nth-child(2)
bilibili.com###bannerAd

! bilibili - 首页
###i_cecream > div.palette-button-outer:nth-child(3) > div.palette-button-inner > div.palette-button-wrap > div.primary-btn:nth-child(2)
###i_cecream > div.bili-header.large-header:first-child > div.bili-header__bar:first-child > ul.left-entry:first-child > li.v-popover-wrap:nth-child(2)
###i_cecream > div.bili-header.large-header:first-child > div.bili-header__bar:first-child > ul.left-entry:first-child > li.v-popover-wrap:nth-child(7)
bilibili.com###i_cecream > main.bili-layout:nth-child(2) > section.bili-grid:nth-child(3)
bilibili.com###i_cecream > main.bili-layout:nth-child(2) > section.bili-grid:nth-child(9) > div.bangumi-area > div.bili-grid.no-margin:last-child > div.bangumi-activity-area
bilibili.com###i_cecream > main.bili-layout:nth-child(2) > section.bili-grid:nth-child(9) > div.bangumi-area > div.bili-grid:first-child
bilibili.com###i_cecream > main.bili-layout:nth-child(2) > section.bili-grid:nth-child(7)
bilibili.com###i_cecream > main.bili-layout:nth-child(2) > section.bili-grid:nth-child(10) > div.guo-chuang-area > div.bili-grid:first-child
bilibili.com###i_cecream > main.bili-layout:nth-child(2) > section.bili-grid:nth-child(10) > div.guo-chuang-area > div.bili-grid.no-margin:last-child
bilibili.com###i_cecream > main.bili-layout:nth-child(2) > section.bili-grid:nth-child(6)
bilibili.com###i_cecream > main.bili-layout:nth-child(2) > section.bili-grid:nth-child(12) > div.variety-area
bilibili.com###i_cecream > main.bili-layout:nth-child(2) > section.bili-grid:nth-child(13)
bilibili.com###i_cecream > main.bili-layout:nth-child(2) > section.bili-grid:nth-child(14)
bilibili.com###i_cecream > main.bili-layout:nth-child(2) > section.bili-grid:nth-child(15)
bilibili.com###i_cecream > main.bili-layout:nth-child(2) > section.bili-grid:nth-child(16)
bilibili.com###i_cecream > main.bili-layout:nth-child(2) > section.bili-grid:nth-child(17)
bilibili.com###i_cecream > main.bili-layout:nth-child(2) > section.bili-grid:nth-child(18)
bilibili.com###i_cecream > main.bili-layout:nth-child(2) > section.bili-grid:nth-child(19)
bilibili.com###i_cecream > main.bili-layout:nth-child(2) > section.bili-grid:nth-child(20)
bilibili.com###i_cecream > main.bili-layout:nth-child(2) > section.bili-grid:nth-child(21)
bilibili.com###i_cecream > main.bili-layout:nth-child(2) > section.bili-grid:nth-child(22)
bilibili.com###i_cecream > main.bili-layout:nth-child(2) > section.bili-grid:nth-child(23)
bilibili.com###i_cecream > main.bili-layout:nth-child(2) > section.bili-grid:nth-child(24)
bilibili.com###i_cecream > main.bili-layout:nth-child(2) > section.bili-grid:nth-child(25)
bilibili.com###i_cecream > main.bili-layout:nth-child(2) > section.bili-grid:nth-child(26)
bilibili.com###i_cecream > main.bili-layout:nth-child(2) > section.bili-grid:nth-child(27)
bilibili.com###i_cecream > main.bili-layout:nth-child(2) > section.bili-grid:nth-child(28)
bilibili.com###i_cecream > main.bili-layout:nth-child(2) > section.bili-grid:nth-child(29)
bilibili.com###i_cecream > main.bili-layout:nth-child(2) > section.bili-grid:nth-child(30)
bilibili.com###i_cecream > main.bili-layout:nth-child(2) > section.bili-grid:nth-child(31)
bilibili.com###i_cecream > main.bili-layout:nth-child(2) > section.bili-grid:nth-child(32)
bilibili.com###i_cecream > main.bili-layout:nth-child(2) > section.bili-grid.no-margin:nth-child(4)
bilibili.com###i_cecream > main.bili-layout:nth-child(2) > section.bili-grid:last-child

! bilibili
bilibili.com###i_cecream > div.channel-container > div.bili-header-default:first-child > div.bili-header.large-header > div.bili-header__bar:first-child > ul.left-entry:first-child > li.v-popover-wrap:nth-child(2)
bilibili.com###i_cecream > div.channel-container > div.bili-header-default:first-child > div.bili-header.large-header > div.bili-header__bar:first-child > ul.left-entry:first-child > li.v-popover-wrap:nth-child(5)
###bili-header-container > div.bili-header > div.bili-header__bar.mini-header > ul.left-entry:first-child > li.v-popover-wrap:nth-child(2)
###internationalHeader > div.mini-header.m-header.mini-type > div.mini-header__content.mini-header--login > div.nav-link:first-child > ul.nav-link-ul.mini > li.nav-link-item:nth-child(2)
###internationalHeader > div.mini-header.m-header.mini-type > div.mini-header__content.mini-header--login > div.nav-link:first-child > ul.nav-link-ul.mini > li.nav-link-item:nth-child(5)
###internationalHeader > div.mini-header.m-header.mini-type > div.mini-header__content.mini-header--login > div.nav-link:first-child > ul.nav-link-ul.mini > li.nav-link-item:nth-child(7)
###internationalHeader > div.mini-header.m-header.mini-type > div.mini-header__content.mini-header--login > div.nav-link:first-child > ul.nav-link-ul.mini > li.nav-link-item:nth-child(6) > span
bilibili.com###i_cecream > div.channel-container > div.bili-header-default:first-child > div.bili-header.large-header > div.bili-header__bar:first-child > ul.left-entry:first-child > li.v-popover-wrap:nth-child(7)
###bili-header-container > div.bili-header > div.bili-header__bar.mini-header > ul.left-entry:first-child > li.v-popover-wrap:nth-child(5)
###bili-header-container > div.bili-header > div.bili-header__bar.mini-header > ul.left-entry:first-child > li.v-popover-wrap:nth-child(7)
bilibili.com###i_cecream > main.bili-layout:nth-child(3) > section.bili-grid:nth-child(3)
bilibili.com###i_cecream > div.adblock-tips:nth-child(2)
bilibili.com###activity_vote

! 苦力怕论坛
mcapks.com##div.container:nth-child(2) > main > div.row:first-child
klpbbs.com###sd > div.sdsd > div.sd_mkes.sd_reply.last_btm:nth-child(8)
klpbbs.com##div.mdui-container:first-child > center:nth-child(2) > div.mdui-chip:first-child
klpbbs.com###ct > div.mn:first-child > div.bm.bw0 > ul.tb.cl:nth-child(2)
klpbbs.com##div.wp.xl.ptm:nth-child(19)
klpbbs.com###chart
klpbbs.118pan.com###page-content > div.row-fluid:last-child > div.downpage_l > div:nth-child(2)
klpbbs.com###comiis_app_block_80
KLPBBS.com###comiis_app_block_75
klpbbs.com##div.container:first-child > div.gg
klpbbs.com##div.container:first-child > div:nth-child(11) > center > botton.btn.btn-primary:nth-child(3)
klpbbs.com##div.container:first-child > p:last-child
klpbbs.com##div#diy_ad.area
mcapks.net##div.col-sm.mt-2

! 石墨文档
shimo.im###layout-top > div.StyledContainer-sc-1YJbeC.cblcqX:nth-child(3)
shimo.im###root > section.Container-sc-33JC-.fzIKxu:first-child > div.ContentContainer-sc-33JC--1.ghuHrr:nth-child(3) > aside.AsideStyled-sc-2sR_uL.dlRmbR:first-child > div.os-host.os-host-foreign.os-theme-dark.os-host-resize-disabled.os-host-scrollbar-horizontal-hidden.os-host-scrollbar-vertical-hidden.Container-sc-2sR_uL-3.fqZdPR.collapsed.os-host-transition:first-child > div.os-padding:nth-child(4) > div.os-viewport.os-viewport-native-scrollbars-invisible > div.os-content > div.ScrollContainer-sc-2sR_uL-4.fdGIrX > nav.StyledNavigation-sc-3Dkbx3.hPwSTR.page-aside-nav:last-child > ol.Container-sc-1y4PVP.ejszDA:first-child > span:last-child
shimo.im###root > div.Page-sc-UZTbA.leuNeO > div.file-page-header:first-child > div.StyledHeader-sc-1VnJGV.cSyWpz.page-header-container:first-child > div.StyledRightHeader-sc-1VnJGV-3.dSsLfp.page-header-right:last-child > div.StyledContainer-sc-1pzPT.hqNpiP.StyledHeadMessage-sc-1VnJGV-2.fUozxI.header-message:first-child
shimo.im##ul._d1jxnq > li._3vpcnm8:first-child
shimo.im##ul._d1jxnq > li._3vpcnm8:nth-child(2)
shimo.im###suite-feedback > div.StyleWrapper-sc-2_LW-5.jKBgRT > div > div._1s2ozb2d:last-child > ul._d1jxnq > li._k48kl2i:nth-child(3)

! 我的世界中国版
mc.163.com##div.wrap:nth-child(6) > div.top_wrap.con:nth-child(3) > div.downland_wrap:first-child > a.age:first-child
mc.163.com###NIE-topBar-recommend
mc.163.com##div.wrap:nth-child(6) > div.top_wrap.con:nth-child(3) > div.downland_wrap:first-child > div.video-section01:last-child

! 比邻云盘
pan.bilnn.com###mfooter > div:first-child > a:nth-child(2)

! MCPEDL
mcpedl.com###main > div.main:nth-child(2) > aside.sidebar:last-child > div.sidebar__wrap:last-child > div.get-vip:nth-child(4)

! 西瓜视频
ixigua.com##div:last-child > div.xg-notification.loginBenefitNotification.xg-notification-anmi-appear-done.xg-notification-anmi-enter-done
ixigua.com###App > div.v3-app-layout.layoutstatus-header--Normal.layoutstatus-side--Normal.categoryPage__layout > div.v3-app-layout__side:nth-child(2) > div.v3-app-layout__side__Normal:last-child > div.v3-app-layout__side__Normal__contentWrapper > div.v3-app-layout__side__wza:nth-child(5)

! 像素工坊
pixelecraft.com###top > div.xb-page-wrapper.xb-canvas-menuActive:nth-child(2) > div.xb-content-wrapper:nth-child(3) > div.p-body:last-child > div.p-body-inner > div.p-body-main.p-body-main--withSidebar:last-child > div.p-body-content:nth-child(3) > div.p-body-pageContent > a:last-child > img

! 其他 - 网盘
app.mediafire.com##div.app-container:first-child > div > div:first-child > div:last-child > div > div > div.rxCustomScroll.rxCustomScrollV.active > div.scrollViewport.scrollViewportV:first-child > div > div:first-child > div:nth-child(3)

! CSBN
blog.csdn.net###asideNewNps
blog.csdn.net###recommendNps
blog.csdn.net###div.hljs-button.signin

! W3Cschool
w3cschool.cn###topbanner
w3cschool.cn###rfbanner
w3cschool.cn###toolbar > div.tool.app:first-child
w3cschool.cn###toolbar > div.tool.wechat:nth-child(2)
w3cschool.cn###wrapper > main.article-main:nth-child(2) > article.article-main-fl:first-child > div.news-content.article-boxshadow:first-child > action.content-extra:last-child > div.content-extra-qrcode:first-child

! heroicons
heroicons.dev###__next > section.relative:nth-child(2) > nav.hidden.lg\:block.p-4.absolute.left-0.top-0:first-child

! 1010jiajiao
1010jiajiao.com###contentA > div.center:last-child
1010jiajiao.com###foot > div.links:first-child

! 必应
cn.bing.com###hdr > div.head_cont:first-child > h1.logo_cont:first-child
cn.bing.com###vs_cont
cn.bing.com###b_footer

! php
php.cn##div.topimages:first-child
php.cn##div.layui-main:nth-child(7) > div.layui-row.php-article > div.layui-col-md4.article-list-right:last-child > div.zwwad:nth-child(3)
php.cn##div.layui-main:nth-child(7) > div.layui-row.php-article > div.layui-col-md8.article-list-left:first-child > a:first-child

! 太平洋电脑
product.pconline.com.cn##div.fixLeftQRcode:nth-child(11)
product.pconline.com.cn###JfixRightBoxJCZ
product.pconline.com.cn###JfixRightBox1
product.pconline.com.cn##div.fixLeftQRcode:nth-child(12)
product.pconline.com.cn###Jwrap > div.lay-ab:nth-child(2) > div.box.box-art:first-child > div.article:first-child > div.artBottomQRcode:nth-child(4)
product.pconline.com.cn###JartBdJCZ
product.pconline.com.cn##div.wrapper:nth-child(4) > div.main.mt10:last-child > div.layC:nth-child(2) > div.modBoxA:last-child
product.pconline.com.cn##div.wrapper:nth-child(4) > div.main.mt10:last-child > div.layAB:first-child > div.clearfix.mod-jcz:last-child

! deepl
deepl.com##header.dl_header.dl_header--sticky.dl_header--has-shadow:nth-child(3) > nav.dl_header_menu_v2.dl_top_element--wide > div.dl_header_menu_v2__items:last-child > div.dl_header_menu_v2__buttons:last-child > div.dl_header_menu_v2__login_button_simple:nth-child(2)
deepl.com###header-pro-button
deepl.com###panelTranslateText > div.lmt__sides_container:nth-child(2) > div.lmt__sides_wrapper:last-child > section.lmt__side_container.lmt__side_container--target:nth-child(3) > div.lmt__textarea_container:nth-child(4) > div:last-child > div.lmt__toolbar_container > div.lmt__target_toolbar > div.lmt_targetToolbar__appPromotion_container_container:last-child > div.lmt_targetToolbar__appPromotion_container
deepl.com###lmt__dict

! 网盘
118pan.com###page-content > div.row-fluid:last-child > div.downpage_l > div:nth-child(2)
pc.woozooo.com###masterdiv

! 腾讯云
cloud.tencent.com###react-root > div:first-child > div.J-body.col-body.pg-2-article:nth-child(6) > div.com-3-layout:last-child > div.layout-side:last-child > section.com-2-panel.side.J-activityRecommend:nth-child(3)
cloud.tencent.com###react-root > div:first-child > div.J-header.c-nav-wrap.c-nav.com-2-nav.fixed:first-child > div.c-nav-mod.c-nav-mod-pc.c-nav-white > div.J-headerBottom.c-nav-bottom.responsive:last-child > div.c-nav-bm-advertising-wrap.J-headerAdvertising:nth-child(3) > a.c-nav-advertising > img:first-child
cloud.tencent.com###react-root > div:first-child > div.J-header.c-nav-wrap.c-nav.com-2-nav.fixed:first-child > div.c-nav-mod.c-nav-mod-pc.c-nav-white > div.J-headerBottom.c-nav-bottom.responsive:last-child > div.c-nav-bm-advertising-wrap.J-headerAdvertising:nth-child(3)

! photopea
photopea.com##div.flexrow.app:nth-child(10) > div:first-child > div:nth-child(3) > div:last-child > div.topbar > button.fitem.bbtn:nth-child(2)

! 其他
###ad-top-double-rectangle
###ad-top-right-rectangle
###ad-search-double-rectangle
###ad-large-sky-scraper
webgradients.com##main.page_wrapper.js-wrapper:first-child > section:first-child > nav.nav > a.g-banner-igtm

! MCwiki
minecraft.fandom.com###mixed-content-footer
minecraft.fandom.com##div.main-container:nth-child(6) > footer.global-footer:last-child > div.global-footer__content:first-child > div:last-child
minecraft.fandom.com##div.main-container:nth-child(6) > footer.global-footer:last-child > div.global-footer__bottom:last-child > div:first-child

! Minebbs
minebbs.com###top > div.p-body:nth-child(4) > div.p-body-inner:last-child > div.uix_contentWrapper:last-child > div.p-body-main.p-body-main--withSidebar > div.p-body-sidebar:last-child > div.uix_sidebarInner:last-child > div.uix_sidebar--scroller > div.swiper-container.swiper-container-initialized.swiper-container-horizontal:nth-child(7)
minebbs.com###top > div.p-body:nth-child(4) > div.p-body-inner:last-child > div.uix_welcomeSection:nth-child(3)
minebbs.com###top > div.p-body:nth-child(4) > div.p-body-inner:last-child > div.uix_contentWrapper:last-child > div.p-body-main.p-body-main--withSidebar > div.p-body-content:first-child > div.samBannerUnit:last-child

! 其他
so.gushiwen.cn###threeWeixin2
