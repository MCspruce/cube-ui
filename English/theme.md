# UI Custom Tutorial
[Home](../#cube-ui-文档)

You need to download a [custom resource pack](https://gitlab.com/MCspruce/cube-ui/-/raw/main/subpack/set.mcpack) to customize the Cube UI.

## Custom Tutorial
! Supported audio file formats: .ogg .fsb
<br>! Best image file format: .png .jpg
<br>! Try to convert the file format to the above mentioned formats

### Modify the click sound
1. open the cube_ui/sounds/ directory of the custom pack
2. put the desired click sound into that directory
3. delete the original click.ogg file
4. rename the desired click sound file to click.ogg

### Modifying cue sound effects
1. open the cube_ui/sounds/ directory of the custom pack
2. put the desired sound effects into the directory
3. delete the original toast.ogg file
4. rename the desired click sound effect file to toast.ogg

### Modify background music
1. open the sounds/music/menu/ directory of the custom pack
2. put the desired background music into the directory
3. rename the desired background music file to menu1.ogg
4. then you can also add menu2.ogg menu3.ogg menu4.ogg to this directory

### Modify the background image
1. Open the textures/ui/ directory of the custom resource pack
2. Place the desired background image into this directory
3. Rename the desired background image file to panorama_overlay.png or panorama_overlay.jpg (keep the original suffix)

### Modify the background image (blurred)
1. open the textures/ui/ directory of the custom resource pack
2. put the desired blurred background image into the directory
3. rename the blurred background image file to panorama_blur.png or panorama_blur.jpg (keep the original suffix)

### Modify variables
1. open the ui/_global_variables.json file of the custom resource pack
2. Modify the values of some variables

## Save the changes and apply

！ Save the changes and apply! To modify the custom pack again and import it into the game
<br>   You need to go to Game > Settings > Storage > Cube UI Custom Bundle > Delete and then import the custom bundle.

1. Go to the directory where the manifest.json file of the custom pack is located
2. Select all the files and folders in the directory
3. compress the selected files and folders
4. change the name of the compressed package to .mcpack
5. Import the .mcpack into Minecraft
6. Go to Minecraft and activate the Cube UI custom resource pack
