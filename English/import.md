# Import Tutorial
[Home](../#cube-ui-文档)

> **Cautions**
> 1. Choose `"Change extension"` or `"Move file"` for zip files and `"Move file"` for folders
> 2. The following method **is only available for Minecraft Keystone Edition**

## Delete the old resource pack section

> **Tip**
> <br>If you have imported Cube UI before, you will need to update the resource package.
> <br>You must update the package to import it again!

Follow this process to delete old UI resource packs
`Go to Minecraft > Settings > Storage > Resource Pack > Select Cube UI > Click Delete`

## Check
Android devices check whether the Minecraft file storage location is set to external
<br>

Windows device checks if file extensions are displayed
<br>`Desktop > My Computer > Organization > Folder and search options > View > Hide extensions of known file types (must be turned off) > Apply`

Check if there is file management that supports renaming

Check the file for completeness
<br>* At least the manifest file is available(manifest.json)
  
Check if the Minecraft version is greater than or equal to 1.17
<br>* No please upgrade Minecraft

## Import
### .mcpack suffix import
#### Windows
One
<br>1 Double click to open
<br>Two
<br>1 Right click on the file to be imported
<br>2 Open method
<br>3 Select Minecraft

#### Other
1 Long click on the .mcpack file
<br>2 Click to share
<br>3 Click to open
<br>4 Select Minecraft

### Move files
#### windows
Move to `C:\Users\[Username]\AppData\Local\Packages\Microsoft.MinecraftUWP_8wekyb3d8bbwe\LocalState\games\com.mojang\resource_packs`

#### android
Move to `/storage/emulated/0/Android/data/com.mojang.minecraftpe/files/games/com.mojang/resource_packs`

#### ios
Move to `我的iphone＞Minecraft＞games＞commojang＞resource_packs`

## Wait

## Activate Resource Pack
**Global**
<br>`Settings > Global Resources > Select Cube UI Resource Pack > Activate > Return`
<br>**World**
<br>`Game > Edit Required World > Resource Pack > Select Cube UI Resource Pack > Activate > Return`