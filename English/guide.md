# UI User guide
[Home](../#cube-ui-文档)

## Container Assist Button
**Drop item button [chest icon]**
<br>Click this button to discard a selected item after selecting it
<br>**Discard all items button [chest icon with plus sign]**
<br>Click this button to discard all the selected items after selecting them
<br>**Destroy item button [fire icon]**
<br>[Visible in Creation Mode] Click this button after selecting an item to destroy the selected one
<br>**Destroy all items button [fire icon with plus sign]**
<br>[Visible in creation mode] Click this button after selecting an item to destroy all of the selected items
<br>**Help/Recipe button [compass icon]**
<br>Click to see the help/recipe for that container
<br>**mode**
<br>Default - No effect
<br>Auto - selected item automatically fills to empty slot (auto grab box/quick move)
<br>Drop - selected item automatically drop all

## Cross-archive replication structure
> **Tip**
<br>Make sure you have admin rights!!!

Additional packages required: [Simple Tools (click to download)](https://gitlab.com/MCspruce/cube-ui/-/raw/main/subpack/简单工具.mcpack)

Exporting
1. type `/give @s structure_block` in the chat bar to get a structure block
2. Place the structure block and select the building structure with the structure block
3. Fill in the name of the structure block on the left and remember it
4. Click the "Export" button in the bottom right corner
5. Place the .mcstructure file in the `structures` folder of the `Simple Tools` (Cube UI add-on package)

Importing
1. Move `Simple Tools` to `(the previous path varies by system)/game/com.mojang/behavior_packs` file
2. Go to Minecraft and activate the "Simple Tools" add-on package in the archive you want to import
3. Enter the world
4. Type `/give @s structure_block` in the chat bar to get a structure block
5. Place the structure block and set the structure block to load mode
6. Fill in the name of the structure block you just entered
7. Set the area where the structure block is to be loaded
8. Click Load

> **Note:**
> * The second time you import, you don't need to move the simplex and activate the simplex, just skip this step! (unless it is not an archive that has been imported before)
> * When exporting the move .mcstructure file for the second time.
    You need to search for "simple tools" in the minecraftWorld folder.
    Then import the .mcstructure file into the structures folder of Simple Tools! (unless it is not a previously imported archive)