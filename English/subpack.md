# Download subpack
[Home](../#cube-ui-文档)

Extensions to enhance the Cube UI, change the theme colors...
<br>Now, choose the right extension pack for you!

> **Tip**
<br>Resource activation is sorted as `Extensions > Cube UI > Other Extensions`
<br>(highest order from the left)

**Dawn Theme**
<br>Author：MC_spruce
<br>Featured：Bring a different kind of dark theme to Cube UI
<br>Link：[Download](https://gitlab.com/MCspruce/cube-ui/-/raw/main/subpack/Cube_UI_Dawn_theme.mcpack)