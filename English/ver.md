# Update log
[Home](../#cube-ui-文档)

## V2.10
Update time: JanuJan 28, 2023 Suggested version: 1.19.51
<br>**[Change]**
<br>> Change focus panel layout
<br>> Change some icons
<br>> Change position of potion effect on Hud screen
<br>**[Fixed]**
<br>✔ Fix the problem of clearing cache overlapping with multi-select button
<br>✔ Fix the problem of low contrast of text on enchantment table

## V2.9.1
Update time: January 22, 2023 Suggested version: 1.19.51
<br>**[Change]**
<br>> Reduce some code size
<br>**[Fixed]**
<br>✔ Improve compatibility

## V2.9
UpdaUp time: January 16, 2023 Suggested version: 1.19.51
<br>**[Add]**
<br>+ Added clear cache button to save options in settings screen
<br>**[Chang]**
<br>> corrected some translations
<br>> changed copy coordinates
<br>**[Fixed]**
<br>✔ Fix the problem that when the crosshair is always displayed and the HUD is hidden, the crosshair is hidden at the same time
<br>✔ Solve some transactions of villagers and merchants, and the slots are not enough Situation
<br>✔ Fix the problem that some function handles of the focus panel, keyboard and mouse users cannot use them

## V2.8.1
Update time：2022.12.30 Suggested version：1.19.51
<br>**[add]**
<br>+ Pause sidebar has been add to the store button
<br>**[fixed]**
<br>✔ Fix the location of the fuel percentage of the brewing table under the classic UI profile
<br>**[subpack]**
<br>* Fix the problem of hiding the llama
<br>**【Please update all Cube UI subpack packages】**

## V2.8
Update time：2022.12.22 Suggested version：1.19.50
<br>**[add]**
<br>+ Warning at the top in case of low durability
<br>+ The backpack screen of the downloadable portable UI profile now supports fast movement
<br>**[change]**
<br>> Changed the click sound and notification sound
<br>> Changed the default background of Cube UI
<br>> Changed the layout of the HUD screen
<br>> Adjusted the position of the toggle button in the focus panel
<br>> Changed the background storage path of Cube UI, see Cube UI customization documentation for details
<br>> Some details optimization
<br>**[fixed]**
<br>✔ Fix the problem that the game screen file reports an error.
<br>**[remove]**
<br>- Remove unnecessary options in Quick Access


## V2.7.2
Update time：2022.12.15 Suggested version：1.19.50
<br>
<br>✔ Improves fluidity


## V2.7.1
Update time：2022.12.10 Suggested version：1.19.50
<br>**[fixed]**
<br>✔ Fix the appearance problem of the enchantment button under the portable version file
<br>✔ The grid will show red when the item is not owned


## V2.7
Update time：2022.12.9 Suggested version：1.19.50
<br>**[fixed]**
<br>✔ Fix the problem of displaying boxes in the archive grid of the portable version in some cases
<br>✔ Incorporate NetEase version incremental package to enhance NetEase version compatibility
<br>✔ Replace the compass ID (only available in the new version)


## V2.6.2
Update time：2022.12.7 Suggested version：1.19.40
<br>**[subpack]**
<br>* Enhanced focus panel expansion pack interface


## V2.6.1
Update time：2022.12.1 Suggested version：1.19.40
<br>**[add]**
<br>+ Added percentage display for some progress bars
<br>**[change]**
<br>> Changed the layout of the focus panel
<br>> Spacing between player name and score in the sidebar scoreboard
<br>> Global variables
<br>  * $quick_pvp changed to $always_auto_place
<br>  * Add $enabled_percentage $spectator_enable variable
<br>  * Remove $start_bottom_text_color $start_partition_color variable
<br>**[fixed]**
<br>✔ Solve the display problem of update log
<br>✔ Fix the display of paper dolls when holding shields in the box interface
<br>**[subpack]**
<br>* Solve the dawn theme text contrast problem


## V2.6
Update time：2022.11.1 Suggested version：1.19.40
<br>**[add]**
<br>+ Add a pause screen exit prompt
<br>+ Add enable exit prompt variable > $enable_quit_tip
<br>**[change]**
<br>> Revamped the content of the User Use Agreement
<br>> Changed hud new touch button style
<br>> Focus panel and hide HUD button moved to pause screen
<br>> Changed chat animation to use new smooth animation
<br>> Rearranged pause screen content
<br>> Changed global variable file content
<br>**[fixed]**
<br>✔ Fixed the issue of not being able to use quick grab boxes in version 1.19.40
<br>✔ Fix the old Minecraft enchantment table GUI issue


## V2.5.1
Update time：2022.10.19 Suggested version：1.19.31
<br>**[change]**
<br>> Pause screen requires a double click in the blank to return to the game
<br>> Adjusted the position of some controls on the hud screen
<br>**[fixed]**
<br>✔ Repair the problem of pop-up panel window when entering the dimension
<br>✔ Fix the problem that the hint will not be hidden when hiding the hud


## V2.5
Update time：2022.10.14 Suggested version：1.19.30
<br>**[add]**
<br>+ Added hide hud button to hud screen (touch screen only)
<br>**[change]**
<br>> Focused menu Toggle on hud display
<br>> Slightly tweaked some details of the hud screen
<br>> Allow to see all commands when entering commands in chat bar
<br>**[subpack]**
<br>* Dark Mode expansion pack is no longer supported (use Dawn theme expansion pack instead)


## V2.4
Update time：2022.10.1 Suggested version：1.19.22
<br>**[add]**
<br>+ You can now search for resources on the Global Resources, Behavior Pack, and Resource Pack pages
<br>+ Pause screen to return to the game by clicking on the blank area
<br>+ Click 'Save and Exit' when you need to click again to exit
<br>**[fixed]**
<br>✔ Fix retroactive pointer display issue in HUD screen
<br>**[remove]**
<br>- Clicking on the paper doll in the start screen will not open the dressing room screen anymore


## V2.3
Update time：2022.9.18 Suggested version：1.19.22
<br>**[change]**
<br>> Some optimizations have been made to the focus panel
<br>> The entrance of the focus panel is changed to open by clicking on the top right corner of the hud
<br>(Handle, new touch method and Window player need to modify RP/ui/_setting.json file to replace the focus panel)
<br>> Compatible with v1.18.0 - beta1.19.40
<br>**[add]**
<br>+ Allow trading with unlocked trading items


## V2.2.1
Update time：2022.8.20 Suggested version：1.19.20
<br>**[change]**
<br>> Focus menu moved to pause screen
<br>> Adjust message pop-up time
<br>**[add]**
<br>+ Add UI settings file


## V2.1
Update time：2022.8.20 Suggested version：1.19.20
<br>**[change]**
<br>> Adjusted pause screen animation
<br>> Now you can send messages in the chat screen by changing the line
<br>> Change $use_multiline_input to true in _global_variables.json to change the line feed
<br>> Some details optimization


## V2.0.2
Update time：2022.8.19 Suggested version：1.19.20
<br>**[change]**
<br>> Change some details
<br>**[fixed]**
<br>✔ Fix a problem with the display of the world edit button in the game interface


## V2.0
Update time：2022.8.17 Suggested version：1.19.20
<br>**[add]**
<br>+ New background
<br>+ New command cube interface
<br>+ Start screen disconnection prompt
<br>+ About UI interface with appearance view and FAQ options
<br>+ New backpack interface and container interface
<br>+ HUD shortcut bar quick switch
<br>+ More user-modifiable variables
<br>**[change]**
<br>> About UI top option bar changed to sidebar
<br>> Better layout of Cube UI version description
<br>> New loading wait prompt
<br>> Some details optimization
<br>**[fixed]**
<br>✔ Fix the loading interface background display problem
<br>✔ Fix some problems


## V1.13.1
Update time：2022.7.23 Suggested version：1.19.12
<br>**[fixed]**
<br>✔ Fix the error of displaying coordinates panel at the top of chat


## V1.13
Update time：2022.7.17 Suggested version：1.19.12
<br>**[change]**
<br>> Adapted to version 1.19.10


## V1.12.1
Update time：2022.6.28 Suggested version：1.19.2
<br>**[change]**
<br>> Adapted to version 1.19.0


## V1.11.0
Update time：2022.4.30 Suggested version：1.18.30
<br>**[change]**
<br>> Adapted to version 1.18.30


## V1.10.0 - V1.10.2
Update time：2022.3.17 Suggested version：1.18.12
<br>**[fixed]**
<br>✔ Fix some known issues


## V1.9.2
Update time：2022.3.13 Suggested version：1.18.12
<br>**[fixed]**
<br>✔ Fix some known issues


## V1.9.1
Update time：2022.3.5 Suggested version：1.18.12
<br>**[fixed]**
<br>✔ Fix the problems of the previous version


## V1.9
Update time：2022.3.5 Suggested version：1.18.10
<br>**[add]**
<br>+ Added shortcut buttons to the item bar and containers (drop and destroy item operations)
<br>**[change]**
<br>> Relayout of the loading screen
<br>**[fixed]**
<br>✔ Fixed the offset position of the close button of the container under the classic archive UI
<br>✔ Partial icon modification


## V1.8.1
Update time：2022.2.22 Suggested version：1.18.10
<br>**[fixed]**
<br>✔ Fix the problem of not being able to use the back button when opening containers


## V1.8
Update time：2022.2.16 Suggested version：1.18.10
<br>**[change]**
<br>> Support version 1.18.10
<br>> Correct some translations


## V1.7
Update time：2022.2.1 Suggested version：1.18.2
<br>**[change]**
<br>> After entering the game, the settings screen will not show the storage toggle anymore
<br>> Modify some icons


## V1.6
Update time：2022.1.20 Suggested version：1.18.0
<br>**[add]**
<br>+ The top bar of the start screen shows Minecraft
<br>**[fixed]**
<br>✔ Fix setting switch group display bug


## V1.5.1
Update time：2022.1.12 Suggested version：1.17.30
<br>**[fixed]**
<br>✔ Fix 1.5 version of the potion display bugs


## V1.5
Update time：2022.1.12 Suggested version：1.17.30
<br>**[change]**
<br>> Pop-up window interface change
<br>> Support Education and China versions
<br>> Some details optimization


## V1.4
Update time：2022.1.8 Suggested version：1.17.30
<br>**[add]**
<br>+ When safety corner is enabled, the top bar fills the outside surface of the safety corner
<br>+ Start screen can now click on the player skin to enter the locker room
<br>**[change]**
<br>> In settings, account information in profile content moved to account information panel
<br>> Account information in settings selection field changed to Toggle
<br>**[fixed]**
<br>✔ Fixed the bug that keeps loading after entering the game


## V1.3.1
Update time：2022.1.2 Suggested version：1.17.10
<br>**[add]**
<br>+ About UI interface version log added collapse history update and extension package update
<br>+ Expansion pack information can now view the expansion pack version and author


## V1.3
Update time：2021.12.30 Suggested version：1.16.100
<br>**[change]**
<br>> Some details optimization

## V1.2
Update time：2021.12.30 Suggested version：1.16.100
<br>**[add]**
<br>+ After logging in, click on your avatar to enter the locker room screen
<br>**[remove]**
<br>- Quickly set the option to remove the file storage location
<br>**[change]**
<br>> Click on the avatar to enter the locker room screen after logging in
<br>> Reduce the file size of the background image
<br>> After entering the game, there is no background on the profile screen
<br>> Some detail changes
<br>**[fixed]**
<br>✔ Fix the chat bar fill command button hiding bug


## V1.1
Update time：2021.12.25 Suggested version：1.16.100
<br>**[fixed]**
<br>✔ Fix position offset bug


## V1.0
Update time：2021.12.25 Suggested version：1.16.10
<br>◆ The first version, nothing updated
